#!/bin/bash
ipv4_re="^([0-9]|[1-9][0-9]{1,2})"
ipv4_re+="\.([0-9]|[1-9][0-9]{1,2})"
ipv4_re+="\.([0-9]|[1-9][0-9]{1,2})"
ipv4_re+="\.([0-9]|[1-9][0-9]{1,2})$"
if [[ "${1}" =~ $ipv4_re ]] ; then
if (( ${BASH_REMATCH[1]} <= 255 )) ; then
if (( ${BASH_REMATCH[2]} <= 255 )) ; then
if (( ${BASH_REMATCH[3]} <= 255 )) ; then
if (( ${BASH_REMATCH[4]} <= 255 )) ; then
for (( a = 31 ; a >= 0 ; --a )) ; do
if (( a >= 24 )) ; then
b=${BASH_REMATCH[1]} ; c=$((a-24)) ;
elif (( a >= 16 )) ; then
echo -n ${BASH_REMATCH[1]}.
b=${BASH_REMATCH[2]} ; c=$((a-16)) ;
elif (( a >= 8 )) ; then
echo -n ${BASH_REMATCH[1]}.
echo -n ${BASH_REMATCH[2]}.
b=${BASH_REMATCH[3]} ; c=$((a-8)) ; else
echo -n ${BASH_REMATCH[1]}.
echo -n ${BASH_REMATCH[2]}.
echo -n ${BASH_REMATCH[3]}.
b=${BASH_REMATCH[4]} ; c=a ; fi
echo -n $(( ( b & ( 0xff & ( 0xff << c ) ) ) ^ ( 1 << c ) ))
if (( a >= 24 )) ; then
echo -n ".0.0.0/$((32-a)), " ;
elif (( a >= 16 )) ; then
echo -n ".0.0/$((32-a)), " ;
elif (( a >= 8 )) ; then
echo -n ".0/$((32-a)), " ;
elif (( a >= 1 )) ; then
echo -n "/$((32-a)), " ;
else echo /32 ; fi
done ; fi ; fi ; fi ; fi ;fi
